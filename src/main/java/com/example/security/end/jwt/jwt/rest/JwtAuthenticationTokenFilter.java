package com.example.security.end.jwt.jwt.rest;

import com.example.security.end.jwt.entity.User;
import com.example.security.end.jwt.jwt.service.JwtService;
import com.example.security.end.jwt.jwt.service.UserServiceJwt;
import com.example.security.end.jwt.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class JwtAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {


    private final static String TOKEN_HEADER = "authorization";

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {


        HttpServletRequest httpServletRequest  = (HttpServletRequest) req;

        // get jwt by token header
        String authorToken = httpServletRequest.getHeader(TOKEN_HEADER);

        // check validate jwt
        if (jwtService.validateTokenLogin(authorToken)){

            String username = jwtService.getUsernameFromToken(authorToken);
            User user = userRepository.findByUsername(username);
            if (user!=null){

                boolean enabled = true;
                boolean accountNonExpired = true;
                boolean credentialsNonExpired = true;
                boolean accountNonLocked = true;

                UserDetails userDetails = new org.springframework.security.core.userdetails.User(
                    username,user.getPassword(),enabled,accountNonExpired,
                        credentialsNonExpired,accountNonLocked,user.getAuthorities()
                );

                UsernamePasswordAuthenticationToken authenticationFilter = new UsernamePasswordAuthenticationToken(
                    userDetails,null,userDetails.getAuthorities()
                );

                authenticationFilter.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authenticationFilter);

            }
        }

        chain.doFilter(req,res);
    }
}
