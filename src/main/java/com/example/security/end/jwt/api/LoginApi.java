package com.example.security.end.jwt.api;

import com.example.security.end.jwt.dto.UserDTO;
import com.example.security.end.jwt.entity.User;
import com.example.security.end.jwt.jwt.service.JwtService;
import com.example.security.end.jwt.jwt.service.UserServiceJwt;
import com.example.security.end.jwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
public class LoginApi {

    @Autowired
    private UserService userService;

    @Autowired
    private UserServiceJwt userServiceJwt;

    @Autowired
    private JwtService jwtService;

    @PostMapping("/signIn")
    public ResponseEntity<String> authentication(@RequestBody UserDTO userDTO){

        String result = "";
        HttpStatus httpStatus=null;

        try {
            if (userServiceJwt.checkLogin(userDTO)){
                result = jwtService.generateTokenLogin(userDTO.getUsername());
                httpStatus = HttpStatus.OK;
            }else {
                result = "Wrong userId and password";
                httpStatus = HttpStatus.BAD_REQUEST;
            }
        }catch (Exception e){
            result = "Server Error";
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<String>(result,httpStatus);
    }

    @PostMapping("/signUp")
    public ResponseEntity<User> signUp(@RequestBody UserDTO userDTO ){
       return ResponseEntity.ok(userService.add(userDTO));
    }

}
