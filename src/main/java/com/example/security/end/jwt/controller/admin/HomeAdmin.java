package com.example.security.end.jwt.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeAdmin {

    @GetMapping("/admin")
    public String adminHome(){
        return "admin/admin-home-page";
    }


}
