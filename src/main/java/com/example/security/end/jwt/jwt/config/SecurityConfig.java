package com.example.security.end.jwt.jwt.config;

import com.example.security.end.jwt.jwt.rest.CustomAccessDeniedHandler;
import com.example.security.end.jwt.jwt.rest.JwtAuthenticationTokenFilter;
import com.example.security.end.jwt.jwt.rest.RestAuthenticationEntrypoint;
import com.example.security.end.jwt.security.CustomSuccessHandler;
import com.example.security.end.jwt.security.UserDetailsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public ModelMapper modelMapper (){
        return new ModelMapper();
    }

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter () throws Exception{
        JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter();
        jwtAuthenticationTokenFilter.setAuthenticationManager(authenticationManager());
        return jwtAuthenticationTokenFilter;
    }

    @Bean
    public RestAuthenticationEntrypoint restAuthenticationEntrypoint(){
        return new RestAuthenticationEntrypoint();
    }

    @Bean
    public CustomAccessDeniedHandler customAccessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }


    protected void configure(HttpSecurity http) throws Exception {

        // disable all url start with /api/**
        http.csrf().ignoringAntMatchers("/api/**");

        http.authorizeRequests().antMatchers("/api/signIn**").permitAll();

        http.antMatcher("/api/**").httpBasic().authenticationEntryPoint(restAuthenticationEntrypoint()).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
            .antMatchers(HttpMethod.GET,"/api/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
            .antMatchers(HttpMethod.POST,"/api/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers(HttpMethod.DELETE,"/api/**").access("hasRole('ROLE_ADMIN')").and()
            .addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class)
            .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler());

        //---------------------------------------------------
        http
                .authorizeRequests()
                .antMatchers("/","/index").permitAll()
                .antMatchers("admin").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginProcessingUrl("/j_spring_security_check")
                .loginPage("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(customSuccessHandler)
                .failureUrl("/login?error")
                .and()
                .exceptionHandling()
                .accessDeniedPage("/403");
    }



    //-----------------------------------------------------------------------------------
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private CustomSuccessHandler customSuccessHandler;

    @Bean
    public PasswordEncoder passwordEncoderSecurity() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)throws Exception{
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoderSecurity());
    }


}
