package com.example.security.end.jwt.jwt.service;

import com.example.security.end.jwt.dto.UserDTO;
import com.example.security.end.jwt.entity.User;
import com.example.security.end.jwt.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceJwt {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public User loadUserByUsername(String userName){
        return userRepository.findByUsername(userName);
    }

    // check user name , password
    public boolean checkLogin(UserDTO userDTO){
        User user = userRepository.findByUsername(userDTO.getUsername());
        if (user!= null){
            if (encoder.matches(userDTO.getPassword(),user.getPassword())) return true;
        }
        return false;
    }

}
