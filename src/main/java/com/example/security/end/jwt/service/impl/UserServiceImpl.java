package com.example.security.end.jwt.service.impl;

import com.example.security.end.jwt.dto.UserDTO;
import com.example.security.end.jwt.entity.Role;
import com.example.security.end.jwt.entity.User;
import com.example.security.end.jwt.repository.RoleRepository;
import com.example.security.end.jwt.repository.UserRepository;
import com.example.security.end.jwt.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private  UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public UserDTO findById(Long id) {
        return converToDTO(userRepository.getOne(id));
    }

    @Override
    public List<UserDTO> getAll() {

        List<UserDTO> list = new ArrayList<>();
        List<User> listEntity = userRepository.findAll();
        for (User u:listEntity
             ) {
            UserDTO userDTO = converToDTO(u);
            list.add(userDTO);
        }

        return list;
    }

    @Override
    public User update(UserDTO userDTO) {
        User user = converToEntity(userDTO);
        User user1 = userRepository.save(user);
        return user1;
    }

    @Override
    public User add(UserDTO userDTO) {
       User user = userRepository.findByUsername(userDTO.getUsername());
       if (user==null){
           user = modelMapper.map(userDTO,User.class);
           user.setPassword(encoder.encode(userDTO.getPassword()));
           Role role = roleRepository.findByName("ROLE_MEMBER");
           Set<Role> roles = new HashSet<>();
           roles.add(role);
           user.setRoles(roles);
           return userRepository.save(user);
       }
       return user;
    }


    public User converToEntity(UserDTO userDTO){
        User user = modelMapper.map(userDTO,User.class);

        Long [] idRoles = userDTO.getIdRole();
        Set<Role> roles = new HashSet<>();

        for (int i = 0; i <idRoles.length ; i++) {
            Role role = roleRepository.getOne(idRoles[i]);
            roles.add(role);
        }
        user.setRoles(roles);
        return user;
    }

    public UserDTO converToDTO(User user){

        UserDTO userDTO = modelMapper.map(user,UserDTO.class);

        Set<Role> roles = user.getRoles();

        List<String> roleNameDTO = new ArrayList<>();
        Long [] roleIdDTO = new Long[roles.size()];

        int i=0;
        for (Role r:roles
             ) {
            roleIdDTO[i]=r.getId();
            roleNameDTO.add(r.getName());
            i++;
        }
        userDTO.setIdRole(roleIdDTO);
        userDTO.setRoleName(roleNameDTO);
        return userDTO;
    }
}
