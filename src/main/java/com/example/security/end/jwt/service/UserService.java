package com.example.security.end.jwt.service;

import com.example.security.end.jwt.dto.UserDTO;
import com.example.security.end.jwt.entity.User;

import java.util.List;

public interface UserService {


    UserDTO findById(Long id);

    List<UserDTO> getAll();

    User update(UserDTO userDTO);

    User add(UserDTO userDTO);



}
